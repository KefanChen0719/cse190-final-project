import cv2
import numpy as np
import os
from read_config import read_config

map_x = (read_config()["map_size"][0])
map_y = (read_config()["map_size"][1])
MAP_SHAPE = (((map_x * (200 + 4)) + 4), ((map_y * (200 + 4)) + 4 + 800), 3)


safesea = cv2.imread("../img/safesea_200.jpg")
black = cv2.imread("../img/black_200.jpg")
harbour = cv2.imread("../img/harbour_new_200.jpg")
block = cv2.imread("../img/wall_200.jpg")
unsafe = cv2.imread("../img/unsafe_200.jpg")
harbour_robot = cv2.imread("../img/harbour_robot_purple_200.jpg")
safesea_robot = cv2.imread("../img/safesea_robot_purple_200.jpg")
unsafe_robot = cv2.imread("../img/unsafe_robot_new_200.jpg")
rescue = cv2.imread("../img/unsafe_p_only_200.jpg")
unsafe_rescue = cv2.imread("../img/unsafe_p_robot_200.jpg")
harbour_rescue = cv2.imread("../img/harbour_p_robot_200.jpg")
safe_rescue = cv2.imread("../img/safe_p_robot_200.jpg")
rf = cv2.imread("../img/RF.jpg")
zero = cv2.imread("../img/0.jpg")
one = cv2.imread("../img/1.jpg")
two = cv2.imread("../img/2.jpg")
three = cv2.imread("../img/3.jpg")
four = cv2.imread("../img/4.jpg")
five = cv2.imread("../img/5.jpg")
six = cv2.imread("../img/6.jpg")
seven = cv2.imread("../img/7.jpg")
eight = cv2.imread("../img/8.jpg")
nine = cv2.imread("../img/9.jpg")
white = cv2.imread("../img/white_L.jpg")
dfs = cv2.imread("../img/DFS_L.jpg")
bfs = cv2.imread("../img/BFS_L.jpg")
uniform = cv2.imread("../img/uniform_L(1).jpg")
astar = cv2.imread("../img/Astar_L.jpg")
replan = cv2.imread("../img/replan.jpg")

img_map = {
    "UNKNOWN": black,
    "HARBOUR": harbour,
    "SAFESEA": safesea,
    "BLOCK": block,
    "UNSAFE": unsafe,
    "HARBOUR+ROBOT": harbour_robot,
    "SAFESEA+ROBOT": safesea_robot,
    "UNSAFE+ROBOT": unsafe_robot,
    "RESCUE": rescue,
    "UNSAFE+RESCUE": unsafe_rescue,
    "HARBOUR+RESCUE": harbour_rescue,
    "SAFESEA+RESCUE": safe_rescue
}

height, width, layers = MAP_SHAPE

def save_image_for_iteration(policy_list, iteration, fuel, label, re):
    print iteration
    #Creating an empty map of white spaces
    empty_map = np.zeros(MAP_SHAPE)
    empty_map.fill(255)
    for row in range(len(policy_list)):
        for col in range(len(policy_list[0])):
            new_pos_row = ((row + 1) * 4) + (row * 200)
            new_pos_col = ((col + 1) * 4) + (col * 200)
            empty_map[new_pos_row : new_pos_row + 200, new_pos_col : new_pos_col + 200] = img_map[policy_list[row][col]]
    empty_map[0 : 200, width - 801 : width - 1] = rf
    tenth = fuel / 10
    oneth = fuel % 10
    empty_map[200 : 800, width - 801 : width - 401] = find_image(tenth)
    empty_map[200 : 800, width - 401 : width - 1] = find_image(oneth)
    empty_map[800 : 1400, width - 801 : width - 1] = label_image(label)
    empty_map[1400 : 2000, width - 801 : width - 1] = replan_image(re)
    cv2.imwrite("../saved_video/iteration_" + str(iteration) + ".jpg", empty_map)

def find_image(num):
    if num == 0:
        return zero
    if num == 1:
        return one
    if num == 2:
        return two
    if num == 3:
        return three
    if num == 4:
        return four
    if num == 5:
        return five
    if num == 6:
        return six
    if num == 7:
        return seven
    if num == 8:
        return eight
    if num == 9:
        return nine

def label_image(label):
    if label == 0:
        return white
    if label == 1:
        return dfs
    if label == 2:
        return bfs
    if label == 3:
        return uniform
    if label == 4:
        return astar

def replan_image(re):
	if re == 0:
		return white
	if re == 1:
		return replan

def generate_video(no_of_iterations):
    video = cv2.VideoWriter("../saved_video/video.avi", cv2.cv.CV_FOURCC('m', 'p', '4', 'v'), 2, (width, height))

    for i in range(no_of_iterations):
        print i
        file_name = "../saved_video/iteration_" + str(i) + ".jpg"
        img = cv2.imread(file_name)
        video.write(img)
        #This removes the image after stitching it to the video. Please comment this if you want the images to be saved
        os.remove(file_name)
    video.release()
