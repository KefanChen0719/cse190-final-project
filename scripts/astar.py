#!/usr/bin/env python

import rospy
import heapq
from cse_190_final_project.msg import MapList

class AStar():
    def __init__(self, pos, policy, publisher, requester):
    	self.pos = pos
    	self.policy = policy
    	self.publisher = publisher
    	self.requester = requester
    	self.row = 10
    	self.col = 10
    	self.safe_cost = 1
    	self.unsafe_cost = 10
        self.fuel = 99

    def search(self):
        self.rescue = [4, 2]
        self.picked = False
        self.goal = self.rescue
    	self.start_search()
        self.goal = [1, 0]
        self.start_search()
        self.rescue = [5, 7]
        self.picked = False
        self.goal = self.rescue
        self.start_search()
        self.goal = [8, 9]
        self.start_search()
    	return self.pos

    def start_search(self):
        while True:
            self.weather = self.requester().data
            for index in range(len(self.weather)):
                if self.weather[index] == 'u':
                    if self.policy[index] == 'SAFESEA':
                        self.policy[index] = "UNSAFE"
                if self.weather[index] == 's':
                    if self.policy[index] == "UNSAFE":
                        self.policy[index] = "SAFESEA"
            self.publish_result(False)
            path = self.set_path()
            cost = 0
            if self.policy[self.getIndex(self.pos)] == "SAFESEA":
                cost = self.safe_cost
            if self.policy[self.getIndex(self.pos)] == "HARBOUR":
                cost = self.safe_cost
            if self.policy[self.getIndex(self.pos)] == "UNSAFE":
                cost = self.unsafe_cost
            self.fuel -= cost
            self.pos = path[0]
            if self.pos == self.goal:
                self.picked = True
                if self.goal == [1, 0] or self.goal == [8, 9]:
                    self.fuel = 99
                self.publish_result(False)
                break
            self.publish_result(False)
            cost = 0
            if self.policy[self.getIndex(self.pos)] == "SAFESEA":
                cost = self.safe_cost
            if self.policy[self.getIndex(self.pos)] == "HARBOUR":
                cost = self.safe_cost
            if self.policy[self.getIndex(self.pos)] == "UNSAFE":
                cost = self.unsafe_cost
            self.fuel -= cost
            self.pos = path[1]
            if self.pos == self.goal:
                self.picked = True
                if self.goal == [1, 0] or self.goal == [8, 9]:
                    self.fuel = 99
                self.publish_result(False)
                break
            self.publish_result(False)
        return self.pos

    def set_path(self):
        start = self.pos
        comefrom = [[0, 0] for i in range(self.row * self.col)]
        f_value = [0 for i in range(self.row * self.col)]
        for i in range(self.row * self.col):
            f_value[i] = abs(i / self.col - start[0]) + abs(i % self.col - start[1])
        value = [0 for i in range(self.row * self.col)]
        t_value = [f_value[i] for i in range(self.row * self.col)]
        value[self.getIndex(start)] = 0
        to_explore = []
        explored = []
        heapq.heappush(to_explore, (t_value[self.getIndex(start)], start))
        while len(to_explore) > 0:
            (val, popped) = heapq.heappop(to_explore)
            if popped in explored:
                continue
            explored.append(popped)
            if popped == self.goal:
                break
            cost = 0
            if self.policy[self.getIndex(popped)] == "SAFESEA":
                cost = self.safe_cost
            if self.policy[self.getIndex(popped)] == "HARBOUR":
                cost = self.safe_cost
            if self.policy[self.getIndex(popped)] == "UNSAFE":
                cost = self.unsafe_cost
            grids = self.get_adjacent(popped)
            for grid in grids:
                if grid not in explored:
                    if (t_value[self.getIndex(grid)], grid) in to_explore:
                        if value[self.getIndex(grid)] > value[self.getIndex(popped)] + cost:
                            value[self.getIndex(grid)] = value[self.getIndex(popped)] + cost
                            t_value[self.getIndex(grid)] = value[self.getIndex(grid)] + f_value[self.getIndex(grid)]
                            comefrom[self.getIndex(grid)] = popped
                    else:
                        value[self.getIndex(grid)] = value[self.getIndex(popped)] + cost
                        t_value[self.getIndex(grid)] = value[self.getIndex(grid)] + f_value[self.getIndex(grid)]
                        comefrom[self.getIndex(grid)] = popped
                    heapq.heappush(to_explore, (t_value[self.getIndex(grid)], grid))
        curr = self.goal
        path = []
        while curr != self.pos:
            path.append(curr)
            curr = comefrom[self.getIndex(curr)]
        path.reverse()
        self.publish_result(True)
        return path

    def get_adjacent(self, pos):
        adjacent = []
        move_list = [[-1, 0], [1, 0], [0, -1], [0, 1]]
        for move in move_list:
            new_pos = [pos[0] + move[0], pos[1] + move[1]]
            if new_pos[0] < 0 or new_pos[0] >= self.row:
                continue
            if new_pos[1] < 0 or new_pos[1] >= self.col:
                continue
            if self.policy[self.getIndex(new_pos)] == "BLOCK":
                continue
            adjacent.append(new_pos)
        return adjacent

    def getIndex(self, pos):
    	return pos[0] * self.col + pos[1] 

    def publish_result(self, re):
        index = self.getIndex(self.pos)
        if not self.picked:
            self.policy[index] += "+ROBOT"
        else:
            self.policy[index] += "+RESCUE"
        index = self.getIndex(self.rescue)
        if not self.picked:
            self.policy[index] = "RESCUE"
        pub = MapList()
        pub.data = self.policy
        pub.data.append(str(self.fuel))
        pub.data.append(str(4))
        if re:
            pub.data.append(str(1))
        else:
            pub.data.append(str(0))
        self.publisher.publish(pub)
        index = self.getIndex(self.pos)
        if self.policy[index] == "HARBOUR+ROBOT" or self.policy[index] == "HARBOUR+RESCUE":
            self.policy[index] = "HARBOUR"
        if self.policy[index] == "SAFESEA+ROBOT" or self.policy[index] == "SAFESEA+RESCUE":
            self.policy[index] = "SAFESEA"
        if self.policy[index] == "UNSAFE+ROBOT" or self.policy[index] == "UNSAFE+RESCUE":
            self.policy[index] = "UNSAFE"
        index = self.getIndex(self.rescue)
        if not self.picked:
            self.policy[index] = "UNSAFE"
        rospy.sleep(1)  
        self.policy.pop()
        self.policy.pop()
        self.policy.pop()