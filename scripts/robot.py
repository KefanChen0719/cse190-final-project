#!/usr/bin/env python

#robot.py implementation goes here

import rospy
import heapq
from read_config import read_config
from dfs import DFS
from bfs import BFS
from uniform_cost import UniformCost
from astar import AStar
from std_msgs.msg import String, Float32, Bool
from cse_190_final_project.msg import MapList
from cse_190_final_project.srv import weatherService

class Robot():
    def __init__(self):
    	self.config = read_config()
    	rospy.init_node("robot")
        self.policy_list_publisher = rospy.Publisher( 
            "/results/policy_list",
            MapList,
            queue_size = 10
        )
        self.sim_complete_publisher = rospy.Publisher( 
            "/map_node/sim_complete",
            Bool,
            queue_size = 10
        )
        self.weather_requester = rospy.ServiceProxy(
        	"weatherService",
        	weatherService
        )
        rospy.sleep(1)
        self.row, self.col = self.config["map_size"]
        self.fuel = 99
        self.initiailize_map()
        self.dfs = DFS(self.pos, self.policy, self.policy_list_publisher)
        self.pos = self.dfs.search()
        self.bfs = BFS(self.pos, self.policy, self.policy_list_publisher)
        self.pos = self.bfs.search()
        self.uniform = UniformCost(self.pos, self.policy, self.policy_list_publisher, self.weather_requester)
        self.pos = self.uniform.search()
        self.astar = AStar(self.pos, self.policy, self.policy_list_publisher, self.weather_requester)
        self.pos = self.astar.search()
        self.finished = Bool()
        self.finished.data = True
        self.sim_complete_publisher.publish(self.finished)
        rospy.sleep(60)
        rospy.signal_shutdown("All steps finished")

    def initiailize_map(self):
    	self.policy = ["UNKNOWN" for i in range(self.row * self.col)]
    	for i in range(self.col):
    		self.policy[self.getIndex([i, 0])] = "BLOCK"
    		self.policy[self.getIndex([0, i])] = "BLOCK"
    		self.policy[self.getIndex([i, self.col - 1])] = "BLOCK"
    		self.policy[self.getIndex([self.row - 1, i])] = "BLOCK"
        self.policy[self.getIndex([1, 0])] = "HARBOUR"
        self.policy[self.getIndex([8, 9])] = "HARBOUR"
        pub = MapList()
        pub.data = self.policy
        pub.data.append(str(0))
        pub.data.append(str(0))
        pub.data.append(str(0))
        self.policy_list_publisher.publish(pub)
        rospy.sleep(1)
        self.policy.pop()
        self.policy.pop()
        self.policy.pop()
        self.pos = [1, 0]
        #for i in range(1, self.row - 1):
        	#for j in range(1, self.col - 1):
        		#self.policy[self.getIndex([i, j])] = "SAFESEA"
        #self.policy[self.getIndex([1, 5])] = "BLOCK"
        #self.policy[self.getIndex([3, 4])] = "BLOCK"
        #self.policy[self.getIndex([4, 6])] = "BLOCK"
        #self.policy[self.getIndex([7, 2])] = "BLOCK"
        #self.pos = [8, 9]
        self.publish_result()

    def getIndex(self, pos):
    	return pos[0] * self.col + pos[1] 

    def publish_result(self):
        index = self.getIndex(self.pos)
        if self.policy[index] == "HARBOUR":
            self.policy[index] = "HARBOUR+ROBOT"
        if self.policy[index] == "SAFESEA":
            self.policy[index] = "SAFESEA+ROBOT"
        if self.policy[index] == "UNSAFE":
        	self.policy[index] = "UNSAFE+ROBOT"
        pub = MapList()
        pub.data = self.policy
        pub.data.append(str(self.fuel))
        pub.data.append(str(0))
        pub.data.append(str(0))
        self.policy_list_publisher.publish(pub)
        if self.policy[index] == "HARBOUR+ROBOT":
            self.policy[index] = "HARBOUR"
        if self.policy[index] == "SAFESEA+ROBOT":
            self.policy[index] = "SAFESEA"
        if self.policy[index] == "UNSAFE+ROBOT":
        	self.policy[index] = "UNSAFE"
        rospy.sleep(1)   
        self.policy.pop()
        self.policy.pop()
        self.policy.pop()

if __name__ == '__main__':
    r = Robot()