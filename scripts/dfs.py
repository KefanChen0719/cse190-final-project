#!/usr/bin/env python

import rospy
from scanner import Scanner
from cse_190_final_project.msg import MapList

class DFS():
    def __init__(self, pos, policy, publisher):
    	self.pos = pos
    	self.policy = policy
    	self.publisher = publisher
    	self.row = 10
    	self.col = 10
        self.fuel = 99
    	self.moveList = [[-1, 0], [1, 0], [0, -1], [0, 1]]
        self.scanner = Scanner()
        self.to_explore = []
    	self.clouded = []
    	for i in range(1, self.row - 1):
    		for j in range(1, self.col - 1):
    			self.to_explore.append([i, j])
    			self.clouded.append([i, j])
    	self.comefrom = [[0, 0] for i in range(self.row * self.col)]

    def search(self):
    	self.publish_result()
        self.scan()
    	while len(self.to_explore) > 0:
    	    move = self.findNextmove()
    	    new_pos = [self.pos[0] + move[0], self.pos[1] + move[1]]
    	    if self.comefrom[self.getIndex(new_pos)] == [0, 0]:
                self.comefrom[self.getIndex(new_pos)] = self.pos
    	    self.pos = new_pos
            self.fuel -= 1
    	    self.publish_result()
    	    self.scan()
    	return self.pos

    def getIndex(self, pos):
    	return pos[0] * self.col + pos[1]

    def findNextmove(self):
    	for dest in self.to_explore:
    		if self.check_nearby(dest):
    			self.to_explore.remove(dest)
    			move = [dest[0] - self.pos[0], dest[1] - self.pos[1]]
    			return move
    	dest = self.comefrom[self.getIndex(self.pos)]
    	move = [dest[0] - self.pos[0], dest[1] - self.pos[1]]
    	return move

    def check_nearby(self, dest):
    	diff = [dest[0] - self.pos[0], dest[1] - self.pos[1]]
    	if diff[0] == 0 and diff[1] == 1:
    		return True
    	if diff[0] == 0 and diff[1] == -1:
    		return True
    	if diff[0] == 1 and diff[1] == 0:
    		return True
    	if diff[0] == -1 and diff[1] == 0:
    		return True
    	return False

    def scan(self):
    	for move in self.moveList:
    		new_pos = [self.pos[0] + move[0], self.pos[1] + move[1]]
    		if self.check_valid(new_pos):
    			if new_pos in self.clouded:
    				index = self.getIndex(new_pos)
    				if self.scanner.scan(new_pos):
    					self.policy[index] = "SAFESEA"
    				else:
    					if new_pos in self.to_explore:
    						self.to_explore.remove(new_pos)
    					self.policy[index] = "BLOCK"
    				self.clouded.remove(new_pos)
    				self.publish_result()

    def check_valid(self, pos):
    	if pos[0] <= 0 or pos[0] >= self.row - 1:
    		return False
    	if pos[1] <= 0 or pos[1] >= self.col - 1:
    		return False;
    	return True

    def publish_result(self):
    	index = self.getIndex(self.pos)
    	if self.policy[index] == "HARBOUR":
    		self.policy[index] = "HARBOUR+ROBOT"
    	if self.policy[index] == "SAFESEA":
    		self.policy[index] = "SAFESEA+ROBOT"
    	pub = MapList()
        pub.data = self.policy
        pub.data.append(str(self.fuel))
        pub.data.append(str(1))
        pub.data.append(str(0))
        self.publisher.publish(pub)
    	if self.policy[index] == "HARBOUR+ROBOT":
    		self.policy[index] = "HARBOUR"
    	if self.policy[index] == "SAFESEA+ROBOT":
    		self.policy[index] = "SAFESEA"
    	rospy.sleep(1)
        self.policy.pop()
        self.policy.pop()
        self.policy.pop()