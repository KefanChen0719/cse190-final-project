#!/usr/bin/env python

import rospy
from cse_190_final_project.msg import MapList

class BFS():
    def __init__(self, pos, policy, publisher):
    	self.pos = pos
    	self.policy = policy
    	self.publisher = publisher
    	self.row = 10
    	self.col = 10
        self.fuel = 99
    	self.moveList = [[-1, 0], [1, 0], [0, -1], [0, 1]]

    def init_explore(self):
    	self.comefrom = [[0, 0] for i in range(self.row * self.col)]
    	self.to_explore = []
    	for i in range(self.row):
    		for j in range(self.col):
    			if self.policy[self.getIndex([i, j])] == "SAFESEA":
    				self.to_explore.append([i, j])
    			if self.policy[self.getIndex([i, j])] == "HARBOUR":
    				self.to_explore.append([i, j])

    def search(self):
    	goal = [8, 9]
    	self.explore(goal)
    	goal = [1, 0]
    	self.explore(goal)
    	goal = [8, 9]
    	self.explore(goal)
    	return self.pos

    def explore(self, goal):
    	self.init_explore()
    	self.to_explore.append(self.pos)
    	self.curr_layer = []
    	self.curr_layer.append(self.pos)
    	to_break = False
    	while True:
    		self.next_layer = []
    		for grid in self.curr_layer:
    			if grid == goal:
    				to_break = True
    				break;
    			self.find_next_layer_pos(grid)
    		if to_break:
    			break
    		self.curr_layer = self.next_layer
    	path = []
    	curr_grid = goal
    	while curr_grid != self.pos:
    		path.append(curr_grid)
    		curr_grid = self.comefrom[self.getIndex(curr_grid)]
    	path.reverse()
    	for i in range(len(path)):
    	    self.pos = path[i]
            self.fuel -= 1
            if i == len(path) - 1:
                self.fuel = 99
    	    self.publish_result()

    def getIndex(self, pos):
    	return pos[0] * self.col + pos[1]

    def find_next_layer_pos(self, pos):
    	for move in self.moveList:
    		new_pos = [pos[0] + move[0], pos[1] + move[1]]
    		if self.check_valid(new_pos):
    			if new_pos in self.to_explore:
    				self.to_explore.remove(new_pos)
    				self.comefrom[self.getIndex(new_pos)] = pos
    				self.next_layer.append(new_pos)

    def check_valid(self, pos):
    	if pos[0] < 0 or pos[0] >= self.row:
    		return False
    	if pos[1] < 0 or pos[1] >= self.col:
    		return False;
    	return True

    def publish_result(self):
    	index = self.getIndex(self.pos)
    	if self.policy[index] == "HARBOUR":
    		self.policy[index] = "HARBOUR+ROBOT"
    	if self.policy[index] == "SAFESEA":
    		self.policy[index] = "SAFESEA+ROBOT"
    	pub = MapList()
        pub.data = self.policy
        pub.data.append(str(self.fuel))
        pub.data.append(str(2))
        pub.data.append(str(0))
        self.publisher.publish(pub)
    	if self.policy[index] == "HARBOUR+ROBOT":
    		self.policy[index] = "HARBOUR"
    	if self.policy[index] == "SAFESEA+ROBOT":
    		self.policy[index] = "SAFESEA"
    	rospy.sleep(1)
        self.policy.pop()
        self.policy.pop()
        self.policy.pop()