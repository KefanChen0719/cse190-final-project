#!/usr/bin/env python

import rospy
import random
from cse_190_final_project.srv import weatherService

class Center():
    def __init__(self):
    	rospy.init_node("center")
    	self.weather_service = rospy.Service(
    		"weatherService",
    		weatherService,
    		self.handle_weather_service
    	)
    	self.row = 10
    	self.col = 10
    	random.seed(0)
    	rospy.spin()

    def handle_weather_service(self, request):
    	policy = ""
    	for i in range(self.row * self.col):
            roll = random.uniform(0, 1)
    	    if roll < 0.2:
                policy = policy + "u"
    	    else:
                policy = policy + "s"
    	return policy

if __name__ == '__main__':
    r = Center()
