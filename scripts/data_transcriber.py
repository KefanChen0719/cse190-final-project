#!/usr/bin/env python

import rospy
from std_msgs.msg import String, Float32, Bool
import json
import image_util
from read_config import read_config
from cse_190_final_project.msg import MapList

class RobotLogger():
    def __init__(self):
        rospy.init_node("robot_logger")
        self.policy_result = rospy.Subscriber(
            "/results/policy_list",
            MapList,
            self.handle_mdp_policy_data
        )
        self.simulation_complete_sub = rospy.Subscriber(
            "/map_node/sim_complete",
            Bool,
            self.handle_shutdown
        )
        self.init_files()
        self.config = read_config()
        rospy.spin()

    def init_files(self):
        self.policy_list = []
        self.path_list = []
        self.iteration_number = 0

    def convert_list_to_2d_array(self, policy_list):
        x, y = self.config["map_size"]
        return [policy_list[i : i + y] for i in xrange(0, len(policy_list), y)]

    def handle_mdp_policy_data(self, policy_list):
        self.policy_list.append(policy_list.data)
        self.policy = policy_list.data
        re = int(self.policy[102])
        label = int(self.policy[101])
        fuel = int(self.policy[100])
        self.policy.pop()
        self.policy.pop()
        self.policy.pop()
        data_to_publish = self.convert_list_to_2d_array(self.policy)
        image_util.save_image_for_iteration(data_to_publish, self.iteration_number, fuel, label, re)
        self.iteration_number += 1

    def handle_shutdown(self, message):
        print "sim complete!", message.data
        image_util.generate_video(self.iteration_number)

if __name__ == '__main__':
    rl = RobotLogger()
